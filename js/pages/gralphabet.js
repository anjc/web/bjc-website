// JavaScript Document for Home Page
$(document).ready(function(e) {	
		var html = "";
		$('.windows-main').append(
		'<div class="container">'+
			'<div class="row tab first-tab">'+
				'<div class="col-md-12 down-tab">'+
					'<h1 class="text-center">Alphabet Grec</h1>'+
				'</div>'+
			'</div>'+
			'<div class="row tab other-tab">'+
				'<div class="col-md-12">'+
					'<div class="table-responsive">'+
						'<table id="tab-alphabet" class="table table-bordered table-hover table-striped">'+
							'<thead>'+
								'<tr>'+
									'<th>Majuscule</th>'+
									'<th>Minuscule</th>'+
									'<th>Nom</th>'+
									'<th>Nom Grec</th>'+
									'<th>Translittération</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>'+
							'</tbody>'+
						'</table>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'
		);
		sofia.ajax({
			dataType: 'json',
			url: 'content/alphabets/greek_alphabet.json',
			success: function(data) {

				$(data).each(function() {
					$.each(this, function(k,v) {
						console.log(v.fra_name);
						html += '<tr><td>' + this.capital + '</td>'+
									'<td>' + this.lower + '</td>'+
									'<td>' + this.name + '</td>'+
									'<td>' + this.greek_name + '</td>'+
									'<td>' + this.xlit + '</td>'+
								'</tr>';
					});
					
				});
				$('#tab-alphabet tbody').append(html);
			}
		});
});	
	
